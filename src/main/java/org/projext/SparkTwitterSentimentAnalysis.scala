package org.projext

import com.vader.sentiment.analyzer.SentimentAnalyzer
import org.apache.spark.SparkConf
import org.apache.spark.sql.types._
import org.apache.spark.sql.{Row, SQLContext}
import org.apache.spark.streaming.twitter.TwitterUtils
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.jets3t.service.model.S3Object
import org.jets3t.service.security.{AWSSessionCredentials}
import twitter4j.auth.OAuthAuthorization
import twitter4j.conf.ConfigurationBuilder

import scala.collection.mutable.ListBuffer


object SparkTwitterSentimentAnalysis {

  def main(args: Array[String]) {

    if (args.length < 10) { //check if enough arguments are provided
      System.err.println("Argument Usage : <Tweet observation time in seconds> <amount of hashtags to include in results> <Twitter consumer key> <Twitter consumer secret> <Twitter access token> <Twitter access token secret> <AWS S3 Bucket name to save to> <awsAccessKey> <awsSecretKey> <awsSessionToken>")

      System.exit(1)
    }

    val Array(obsTime, tableLength, consumerKey, consumerSecret, accessToken, accessTokenSecret, awsBucketName, awsAccessKey, awsSecretKey, awsToken) = args.take(10) //get all keys and secrets

    val configurationBuilder = new ConfigurationBuilder
    configurationBuilder.setDebugEnabled(true).setOAuthConsumerKey(consumerKey)
      .setOAuthConsumerSecret(consumerSecret)
      .setOAuthAccessToken(accessToken)
      .setOAuthAccessTokenSecret(accessTokenSecret)
    val auth = new OAuthAuthorization(configurationBuilder.build) //Authorization Unit for twitter


    val sparkConf = new SparkConf().setAppName("TwitterTags")//.setMaster("local[3]")
    val streamingContext = new StreamingContext(sparkConf, Seconds(2)) // Request intervall
    streamingContext.sparkContext.setLogLevel("ERROR")
    val sqlContext = new SQLContext(streamingContext.sparkContext)

    val tweetStream = TwitterUtils.createStream(streamingContext, Some(auth))
      .filter(_.getLang == "en") //Filter by language
      .filter(_.getHashtagEntities.length > 0)//Filter to remove tweets without a hashtag


    val hashtagSentiments = tweetStream.flatMap(tweet => //Flatten the Map of Tuples (Hashtag,CompoundPolarity)
      tweet.getHashtagEntities // Get the HashtagEntities of a Tweet
        .map(_.getText).distinct // Get the text of the distinct hashtags
        .map(hashtag => { // Go through every hashtag
          val analyzer = new SentimentAnalyzer(tweet.getText) // Create SentimentAnalyzer with the actual tweet
          analyzer.analyze() // Analyze  the tweet - Calculation of Polaritys
          (hashtag, List[String](new StringSentiment().getSentiment(analyzer.getPolarity.get("compound")))) // Return Tuple (Hashtag,CompoundPolarity)
        })
    )

    val reducedHashtagSentiments = hashtagSentiments
      .reduceByKeyAndWindow( //Group by hashtags
        (value1, value2) => {
          value1.union(value2)
        }, Seconds(obsTime.toLong) //use x Amount of seconds
      )
      .transform(
        _.sortBy(value => value._2.size, //Sort by amount of tweets
          false //ascending=false
        )
      )

    reducedHashtagSentiments.foreachRDD { rdd =>
      val list = rdd.take(tableLength.toInt) //use top x hashtags

      val rows = new ListBuffer[Row]()

      list.foreach { hashtagTuple =>
        val percentages = new StringSentiment().getPercent(hashtagTuple._2)//Calculate percentage of (Pos,Neg,Neu) tweets
        rows += Row(
          hashtagTuple._1, //hashtag
          hashtagTuple._2.size: Integer, //amountOfTweets
          percentages(0): Float,  //positive percentage
          percentages(1): Float,  //neutral percentage
          percentages(2): Float   //negative percentage
        )
      }

      val rowsToSave = streamingContext.sparkContext.parallelize(rows) //parallelize -> allows working on different partitions of cluster

      val schema = List(  //columns to save
        StructField("hashtag", StringType, true),
        StructField("amountOfTweets", IntegerType, true),
        StructField("posPercent", FloatType, true),
        StructField("neutPercent", FloatType, true),
        StructField("negPercent", FloatType, true)
      )

      val dataFrame = sqlContext.createDataFrame(rowsToSave, StructType(schema))
      dataFrame.show() //Shows first 20 entrys of list in console

      val awsCredentials = new AWSSessionCredentials(awsAccessKey, awsSecretKey, awsToken)

      import org.jets3t.service.impl.rest.httpclient.RestS3Service
      val s3Service = new RestS3Service(awsCredentials)

      val rowArray = dataFrame.rdd.map(row=>row.mkString(",")).collect() //transform row to string separated by ',' and store in array

      s3Service.putObject(s3Service.getBucket(awsBucketName), new S3Object("Top_Hashtags.txt", rowArray.toList.toString)) //store in bucket
    }
    streamingContext.start()
    streamingContext.awaitTermination()
  }
}

class StringSentiment extends Serializable {
  def getSentiment(polarity: Float): String = { //Transforms compound polarity value to string
    if (polarity > 0.05) {
      "POSITIVE"
    }
    else if (polarity > -0.05) {
      "NEUTRAL"
    }
    else {
      "NEGATIVE"
    }
  }

  def getPercent(sentimentArr: List[String]): List[Float] = { //compute the percentages of positive, neutral and negative tweets
    val totalAmount = sentimentArr.length
    var posAmount: Float = 0
    var neutAmount: Float = 0
    var negAmount: Float = 0

    //Count Pos/Neu/Neg
    sentimentArr.foreach { sentiment =>
      if (sentiment.equals("POSITIVE")) {
        posAmount += 1
      }
      if (sentiment.equals("NEUTRAL")) {
        neutAmount += 1
      }
      if (sentiment.equals("NEGATIVE")) {
        negAmount += 1
      }
    }

    //Compute percentage
    val posPercentage = BigDecimal(posAmount / totalAmount * 100f).setScale(2, BigDecimal.RoundingMode.HALF_UP).toFloat
    val neutPercentage = BigDecimal(neutAmount / totalAmount * 100f).setScale(2, BigDecimal.RoundingMode.HALF_UP).toFloat
    val negPercentage = BigDecimal(negAmount / totalAmount * 100f).setScale(2, BigDecimal.RoundingMode.HALF_UP).toFloat

    //Return
    List[Float](posPercentage, neutPercentage, negPercentage)
  }
}


